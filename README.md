# Introduction

This is an application that allows you to view random Chuck norris jokes from the api link https://api.chucknorris.io/jokes/random and gives you an option to save the joke to an SQLite database. 

The first interface screen shows a list of saved jokes. There's an option to delete the any joke by swipping left. To move to the next screen, click the 'Add more jokes' button. A network call is made to the api and a random joke is generated and displayed on the screen. There is an option to save this joke or generate another random joke. 

On the top left corner of the random joke screen, there is a back button that takes you back to the first page showing the list of jokes in the database. 

# Reference links

https://api.chucknorris.io/jokes/random

# Getting started

Install the apk on your IDE or your phone, view and save the jokes you like.

## What's contained in this project

### Android code

JokeListActivity - This activity contains a list view of the jokes saved in the database. To view more jokes or add more jokes to the list, click a botton to navigate to the 'NewJokeActivity'. To delete a joke from the list, swipe left.

NewJokeActivity - This activity shows a single joke downloaded from the api. There is an option to rerun for a new joke or save the displayed joke to the list of jokes.

DownloadService- This is a service that downloads jokes from the api https://api.chucknorris.io/jokes/random

Joke- This is a model of the joke object

JokeAdapter - A cursor adapter that loades the list item in the JokeListActivity class.

JokeDbHelper - An  SQLitehelper class.

JokeProvider - A content provider class that manages the access to the SQLite database.

JokeContract- A contract class that has all the constants`` for the SQLite database 

### Fastlane files

It also has fastlane setup per our [blog post](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/) on
getting GitLab CI set up with fastlane. Note that you may want to update your
fastlane bundle to the latest version; if a newer version is available, the pipeline
job output will tell you.

### Dockerfile build environment

In the root there is a Dockerfile which defines a build environment which will be
used to ensure consistent and reliable builds of your Android application using
the correct Android SDK and other details you expect. Feel free to add any
build-time tools or whatever else you need here.

We generate this environment as needed because installing the Android SDK
for every pipeline run would be very slow.

### Gradle configuration

The gradle configuration is exactly as output by Android Studio except for the
version name being updated to 

Instead of:

`versionName "1.0"`

It is now set to:

`versionName "1.0-${System.env.VERSION_SHA}"`

You'll want to update this for whatever versioning scheme you prefer.

### Build configuration (`.gitlab-ci.yml`)

The sample project also contains a basic `.gitlab-ci.yml` which will successfully 
build the Android application.

Note that for publishing to the test channels or production, you'll need to set
up your secret API key. The stub code is here for that, but please see our
[blog post](https://about.gitlab.com/2019/01/28/android-publishing-with-gitlab-and-fastlane/) for
details on how to set this up completely. In the meantime, publishing steps will fail.

The build script also handles automatic versioning by relying on the CI pipeline
ID to generate a unique, ever increasing number. If you have a different versioning
scheme you may want to change this.

```yaml
    - "export VERSION_CODE=$(($CI_PIPELINE_IID)) && echo $VERSION_CODE"
    - "export VERSION_SHA=`echo ${CI_COMMIT_SHA:0:8}` && echo $VERSION_SHA"
```