package com.example.myfirstapp.activity;

import android.content.ContentValues;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.myfirstapp.R;
import com.example.myfirstapp.data.JokeContract.*;
import com.example.myfirstapp.model.Joke;
import com.example.myfirstapp.utility.Constants;
import com.example.myfirstapp.utility.JsonProcessor;
import com.example.myfirstapp.utility.NetworkUtil;
import com.squareup.picasso.Picasso;

public class NewJokeActivity extends AppCompatActivity {

    private TextView textView;
    private ImageView imageView;
    private Joke joke;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_joke);
        textView = findViewById(R.id.joke_text);
        imageView = findViewById(R.id.iv_joke_icon);
        loadJoke(textView);
    }

    public void loadJoke(View view) {
        NetworkUtil.fetchData(this, Constants.API_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                joke = JsonProcessor.processJson(response);
                textView.setText(joke.getValue());
                Picasso.get().load(joke.getIconUrl()).into(imageView);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                textView.setText("Not working");
            }
        });
    }

    public void saveJoke(View view) {
        ContentValues values = new ContentValues();
        values.put(JokeEntry.COLUMN_EXTERNAL_ID, joke.getExternalID());
        values.put(JokeEntry.COLUMN_ICON_URL, joke.getIconUrl());
        values.put(JokeEntry.COLUMN_JOKE_CONTENT, joke.getValue());
        getContentResolver().insert(JokeEntry.CONTENT_URI, values);
        finish();
    }

}