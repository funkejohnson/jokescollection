package com.example.myfirstapp.activity;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfirstapp.R;
import com.example.myfirstapp.adapter.JokeAdapter;
import com.example.myfirstapp.data.JokeContract.JokeEntry;
import com.example.myfirstapp.data.JokeDbHelper;
import com.example.myfirstapp.model.Joke;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class JokeListActivity extends AppCompatActivity{

    private JokeDbHelper jokeDbHelper;
    private JokeAdapter jokeAdapter;
    private List<Joke> jokes;
    private Cursor cursor;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke_list);
        jokeDbHelper = new JokeDbHelper(this);
        jokes = createJokeList();
        jokeAdapter = new JokeAdapter(jokes, this);
        recyclerView = findViewById(R.id.lv_joke_list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(jokeAdapter);


        FloatingActionButton floatingActionButton = findViewById(R.id.fab_new_joke);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addJoke();
            }
        });
        swipeItem();

    }

    @Override
    protected void onStart() {
        super.onStart();
        jokes.clear();
        jokes = createJokeList();
        jokeAdapter.setJokes(jokes);
    }



    private void addJoke() {
        Intent intent = new Intent(this, NewJokeActivity.class);
        startActivity(intent);
    }

    private List<Joke> createJokeList() {
        List<Joke> jokeList = new ArrayList<>();
        String [] projection = {JokeEntry._ID, JokeEntry.COLUMN_EXTERNAL_ID, JokeEntry.COLUMN_ICON_URL, JokeEntry.COLUMN_JOKE_CONTENT};
        cursor = getContentResolver().query(JokeEntry.CONTENT_URI, projection,null, null, null);
        while(cursor != null && cursor.moveToNext()){
            String value = cursor.getString(cursor.getColumnIndex(JokeEntry.COLUMN_JOKE_CONTENT));
            String uniqueId = cursor.getString(cursor.getColumnIndex(JokeEntry.COLUMN_EXTERNAL_ID));
            String iconUrl = cursor.getString(cursor.getColumnIndex(JokeEntry.COLUMN_ICON_URL));
            int id = cursor.getInt(cursor.getColumnIndex(JokeEntry._ID));
            Joke joke = new Joke(id,value,uniqueId,iconUrl);
            jokeList.add(joke);
        }
        return jokeList;
    }
    private void swipeItem(){
        ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();
                Joke joke = jokes.get(position);
                deleteJoke(joke);
                jokeAdapter.notifyDataSetChanged();
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void deleteJoke(Joke joke) {
        jokes.remove(joke);
        Uri uri = ContentUris.withAppendedId(JokeEntry.CONTENT_URI, joke.getId());
        getContentResolver().delete(uri, null, null);
    }
}
