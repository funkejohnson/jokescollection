package com.example.myfirstapp.model;

import com.google.gson.annotations.SerializedName;

public class Joke {
    private transient int id;
    private String value;
    @SerializedName(value = "id")
    private String externalID;
    @SerializedName(value = "icon_url")
    private String iconUrl;

    public Joke(int id, String value, String externalID, String iconUrl) {
        this.id = id;
        this.value = value;
        this.externalID = externalID;
        this.iconUrl = iconUrl;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }

    public String getExternalID() {
        return externalID;
    }

    public String getIconUrl() {
        return iconUrl;
    }
}
