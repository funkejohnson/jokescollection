package com.example.myfirstapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.myfirstapp.data.JokeContract.*;

import androidx.annotation.Nullable;

public class JokeDbHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;

    private final String CREATE_JOKE_TABLE = "CREATE TABLE " + JokeEntry.TABLE_NAME + " ( "
            + JokeEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + JokeEntry.COLUMN_EXTERNAL_ID + " TEXT NOT NULL UNIQUE, "
            + JokeEntry.COLUMN_ICON_URL + " TEXT, "
            + JokeEntry.COLUMN_JOKE_CONTENT + " TEXT NOT NULL UNIQUE)";

    public JokeDbHelper(@Nullable Context context) {
        super(context, JokeContract.DATABASE_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_JOKE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
