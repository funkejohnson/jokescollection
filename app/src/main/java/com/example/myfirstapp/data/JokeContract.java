package com.example.myfirstapp.data;

import android.net.Uri;
import android.provider.BaseColumns;

public class JokeContract {
    public static final String DATABASE_NAME = "joke.db";
    public static final String CONTENT_AUTHORITY = "com.example.myfirstapp";
    public static final String BASE_URI = "content://" + CONTENT_AUTHORITY;

    public static class JokeEntry implements BaseColumns {
        public static final String PATH = "joke";
        public static final String TABLE_NAME = "joke";
        public static final String COLUMN_EXTERNAL_ID = "unique_id";
        public static final String COLUMN_JOKE_CONTENT = "joke_content";
        public static final String COLUMN_ICON_URL = "icon_url";
        public static final Uri CONTENT_URI = Uri.parse(BASE_URI + "/" + PATH);
    }
}
