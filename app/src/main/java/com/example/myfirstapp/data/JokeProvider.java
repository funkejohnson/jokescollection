package com.example.myfirstapp.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.example.myfirstapp.data.JokeContract.*;

import com.example.myfirstapp.model.Joke;

import java.util.ArrayList;
import java.util.List;

public class JokeProvider extends ContentProvider {

    private static UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    private JokeDbHelper jokeDbHelper;
    private Cursor cursor;
    private static final int JOKE = 100;
    private static final int JOKE_ID = 101;

    static {
        uriMatcher.addURI(JokeContract.CONTENT_AUTHORITY, JokeEntry.PATH, JOKE);
        uriMatcher.addURI(JokeContract.CONTENT_AUTHORITY, JokeEntry.PATH + "/#", JOKE_ID);
    }

    @Override
    public boolean onCreate() {
        jokeDbHelper = new JokeDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase db = jokeDbHelper.getReadableDatabase();
        int matchCode = uriMatcher.match(uri);
        switch (matchCode) {
            case JOKE:
                cursor = db.query(JokeEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            case JOKE_ID:
                selection = JokeEntry._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                cursor = db.query(JokeEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Unknown uri " + uri);
        }
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase db = jokeDbHelper.getWritableDatabase();
        int matchCode = uriMatcher.match(uri);
        Uri returnedUri;

        switch (matchCode) {
            case JOKE:
            case JOKE_ID:
                returnedUri = ContentUris.withAppendedId(uri, db.insert(JokeEntry.TABLE_NAME, null, values));
                break;
            default:
                throw new IllegalArgumentException("Update is not supported for " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnedUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        SQLiteDatabase db = jokeDbHelper.getWritableDatabase();
        int matchCode = uriMatcher.match(uri);
        int deletedRows;
        switch (matchCode){
            case JOKE:
                deletedRows = db.delete(JokeEntry.TABLE_NAME, selection, selectionArgs);
                break;
            case JOKE_ID:
                selection = JokeEntry._ID + "=?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                deletedRows = db.delete(JokeEntry.TABLE_NAME, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException(" Delete is not supported for " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return deletedRows;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    public List<Joke> getJokes() {
        List<Joke> jokes = new ArrayList<>();
        while(cursor.moveToNext()){
            String value = cursor.getString(cursor.getColumnIndex(JokeEntry.COLUMN_JOKE_CONTENT));
            String uniqueId = cursor.getString(cursor.getColumnIndex(JokeEntry.COLUMN_EXTERNAL_ID));
            String iconUrl = cursor.getString(cursor.getColumnIndex(JokeEntry.COLUMN_ICON_URL));
            int id = cursor.getInt(cursor.getColumnIndex(JokeEntry._ID));
            Joke joke = new Joke(id, value, uniqueId, iconUrl);
            jokes.add(joke);
        }
        return jokes;
    }
}
