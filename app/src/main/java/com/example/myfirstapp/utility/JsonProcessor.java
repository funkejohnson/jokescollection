package com.example.myfirstapp.utility;

import com.example.myfirstapp.model.Joke;
import com.google.gson.Gson;

public class JsonProcessor {
    public static Joke processJson(String input){
        Gson gson = new Gson();
        Joke joke = gson.fromJson(input, Joke.class);
        return joke;
    }
}
