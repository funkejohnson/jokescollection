package com.example.myfirstapp.adapter;

import android.content.Context;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myfirstapp.R;
import com.example.myfirstapp.model.Joke;
import com.squareup.picasso.Picasso;

import java.util.List;

public class JokeAdapter extends RecyclerView.Adapter<JokeAdapter.JokeAdapterViewHolder> {
    private List<Joke> jokes;
    private Context context;
    public JokeAdapter(List<Joke> jokes, Context context) {
        this.jokes = jokes;
        this.context = context;
    }

    public void setJokes(List<Joke> jokes) {
        this.jokes = jokes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public JokeAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new JokeAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull JokeAdapterViewHolder holder, int position) {
        Joke joke = jokes.get(position);
        holder.bind(joke);
    }

    @Override
    public int getItemCount() {
        if(jokes == null) {
            return 0;
        }
        return jokes.size();
    }

    public class JokeAdapterViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;
        private ImageView imageView;

        public JokeAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tv_joke);
            imageView = itemView.findViewById(R.id.iv_joke_icon);

            itemView.setOnDragListener(new View.OnDragListener() {
                @Override
                public boolean onDrag(View v, DragEvent event) {
                    return false;
                }
            });
        }
        private void bind(Joke joke) {
            Picasso.get().load(joke.getIconUrl()).into(imageView);
            textView.setText(joke.getValue());
        }
    }
}
